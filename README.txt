This module adds functionality for batch deleting all menu links from a selected menu. This can be useful with very large sites, where for example rebuilding a Taxonomy Menu times out during the menu link deletion phase of the rebuild.

The module goes well together with Taxonomy delete all terms.

After the module is enabled, the menu edit pages (/admin/structure/menu/manage/%/edit) have an extra button labeled "Delete all links". 